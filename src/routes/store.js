import { writable, get } from 'svelte/store';
import gameData from './gameData.js';
const ENEMY_HEALTH_MULTIPLICATOR = 5;
export const PLAYER_ATTACK_SKILL = 0.2;
export const ENEMY_ATTACK_SKILL = 0.1;
export const PLAYER_HEAL_SKILL = 0.2;
export const PLAYER_CRITICAL_SKILL = 0.05;

const DEFAULT_LOG = { id: 1, type: 'info', message: 'The battle begins!' };
export const combatLog = writable([DEFAULT_LOG]);
export function addLogEntry(entry) {
	combatLog.update((log) => [{ ...entry, id: Date.now() + Math.random() }, ...log]);
}

const createBattleStore = () => {
	const { subscribe, set, update } = writable({ turn: 'player', isLocked: false });
	const startTurn = () => {
		update((state) => ({ ...state, isLocked: true }));
	};
	const endTurn = () => {
		update((state) => {
			let turn = state.turn === 'player' ? 'enemy' : 'player';
			let isLocked = state.turn === 'enemy' ? false : true;
			if (state.turn === 'player' && get(enemy).currentHealth) {
				setTimeout(enemy.attack, 0);
			} else if (state.turn === 'player' && !get(enemy).currentHealth) {
				addLogEntry({ type: 'info', message: `${get(enemy).name} defeated!` });
				player.defeatEnemy();
				enemy.newEnemy();
				addLogEntry({ type: 'info', message: `You are fiting with ${get(enemy).name}!` });
				turn = 'player';
				isLocked = false;
			} else if (!get(player).currentHealth) {
				player.gameOver();
			}
			return {
				turn,
				isLocked
			};
		});
	};
	return {
		subscribe,
		set,
		update,
		startTurn,
		endTurn
	};
};
export const battle = createBattleStore();

const initialPlayerState = {
	type: 'player',
	name: 'Player',
	avatar: '1',
	background: gameData.backgrounds[Math.floor(Math.random() * gameData.backgrounds.length)],
	currentHealth: 100,
	maxHealth: 100,
	attack: 0,
	heal: 0,
	dice: 1,
	defeatedEnemies: 0,
	charaterSelected: false,
	skillSelected: true,
	criticalChanse: 0.1
};
const wait = async (time) => {
	await new Promise((resolve) => setTimeout(resolve, time));
};

const createPlayerStore = (data) => {
	const { subscribe, set, update } = writable(data);

	const attack = async () => {
		battle.startTurn();
		const dice = Math.floor(Math.random() * 6) + 1;
		const isCritical = Math.random() < get(data.type === 'player' ? player : enemy).criticalChanse;
		const attack =
			Math.round(
				(get(data.type === 'player' ? player : enemy).attack + dice) * (isCritical ? 2 : 1) * 10
			) / 10;

		update((state) => {
			return { ...state, dice, diceAnimation: true };
		});
		await wait(1000);
		addLogEntry({
			type: data.type,
			message: `${get(data.type === 'player' ? player : enemy).name} attacked ${attack}${
				isCritical ? '. Critical hit x2!' : ''
			}`
		});
		if (data.type === 'player') {
			enemy.attacked(attack, isCritical);
		} else {
			player.attacked(attack, isCritical);
		}
		await wait(500);
		battle.endTurn();
	};

	const heal = async () => {
		battle.startTurn();
		const dice = Math.floor(Math.random() * 6) + 1;

		const heal = Math.round((get(data.type === 'player' ? player : enemy).heal + dice)*10)/10;
		update((state) => ({ ...state, dice, diceAnimation: true }));
		await wait(1500);
		addLogEntry({
			type: 'player',
			message: `${get(data.type === 'player' ? player : enemy).name} heal ${heal}`
		});
		update((state) => ({
			...state,
			healAnimation: true,
			currentHealth:
				state.currentHealth + heal >= state.maxHealth ? state.maxHealth : state.currentHealth + heal
		}));
		await wait(1000);
		battle.endTurn();
	};

	const attacked = (points, isCritical = false) => {
		update((state) => {
			return {
				...state,
				[isCritical ? 'criticalAnimation' : 'attackAnimation']: true,
				currentHealth: state.currentHealth - points <= 0 ? 0 : state.currentHealth - points
			};
		});
	};
	const newEnemy = () => {
		const newEnemy = gameData.enemies[Math.floor(Math.random() * gameData.enemies.length)];
		update((state) => ({
			...state,
			...newEnemy,
			maxHealth: state.maxHealth + ENEMY_HEALTH_MULTIPLICATOR,
			currentHealth: state.maxHealth + ENEMY_HEALTH_MULTIPLICATOR,
			attack: state.attack + ENEMY_ATTACK_SKILL
		}));
	};
	const gameOver = () => {
		update((state) => ({ ...state, gameOver: true }));
	};
	const replay = () => {
		if (data.type === 'player') {
			combatLog.set([DEFAULT_LOG]);
			update((state) => ({
				...state,
				background: gameData.backgrounds[Math.floor(Math.random() * gameData.backgrounds.length)],
				currentHealth: 100,
				gameOver: false,
				defeatedEnemies: 0,
				charaterSelected: false,
				maxHealth: 100,
				attack: 0,
				heal: 0,
				criticalChanse: 0.1
			}));
		}
		if (data.type === 'enemy') {
			enemy.newEnemy();
			update((state) => ({ ...state, currentHealth: 10, maxHealth: 10, attack: 0 }));
		}
	};
	const defeatEnemy = () => {
		update((state) => ({
			...state,
			defeatedEnemies: state.defeatedEnemies + 1,
			skillSelected: false
		}));
	};
	const selectAvatar = ({ avatar, name }) => {
		update((state) => ({ ...state, avatar, name }));
	};
	const start = () => {
		update((state) => ({ ...state, charaterSelected: true }));
	};
	return {
		set,
		update,
		subscribe,
		attack,
		heal,
		attacked,
		newEnemy,
		gameOver,
		replay,
		defeatEnemy,
		selectAvatar,
		start
	};
};

export const player = createPlayerStore(initialPlayerState);

const enemyData = {
	type: 'enemy',
	currentHealth: 10,
	maxHealth: 10,
	attack: 0,
	heal: 0,
	criticalChanse: 0
};

export let enemy = createPlayerStore({
	...enemyData,
	...gameData.enemies[Math.floor(Math.random() * gameData.enemies.length)]
});
