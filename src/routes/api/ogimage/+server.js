import { createCanvas, loadImage } from 'canvas';

async function generateResultImage(
	avatarUrl,
	backgroundUrl,
	playerName,
	defeatedEnemies,
	gameName
) {
	const canvasWidth = 800;
	const canvasHeight = 600;
	const canvas = createCanvas(canvasWidth, canvasHeight);
	const ctx = canvas.getContext('2d');
	const x = canvas.width / 2;

	// Load and draw the background image
	const backgroundImage = await loadImage(backgroundUrl);
	ctx.drawImage(backgroundImage, 0, 0, backgroundImage.width, backgroundImage.height);

	ctx.fillStyle = 'rgba(0, 0, 0, 0.6)';
	ctx.fillRect(0, 0, 800, 600);

	// Load and draw the avatar image
	const avatarImage = await loadImage(avatarUrl);
	ctx.drawImage(avatarImage, 300, 156, 200, 200);
	ctx.textAlign = "center"
	// Draw game name
	ctx.font = '80px fantasy';
	ctx.fillStyle = 'white';
	ctx.fillText(gameName, x, 90);

	// Draw player name
	ctx.font = '60px fantasy';
	ctx.fillStyle = 'white';
	ctx.fillText(playerName, x, 420);

	// Draw defeated enemies counter
	
    ctx.font = '60px fantasy';
    ctx.fillStyle = 'white';
  if(defeatedEnemies !==''){
    ctx.fillText(`Score: ${defeatedEnemies}`, x, 500);
  } else {
    ctx.fillText(`Play now!`, x, 500);
  }


	// Save the image to a file
	const buffer = canvas.toBuffer('image/png');
	return buffer;
	// fs.writeFileSync('result.png', buffer);
}

export async function GET(data) {
  const params = data;
	const { origin, searchParams } = params.url;

	const avatarUrl = `${origin}/images/player/${searchParams.get('a') || 1}.jpeg`;
	const backgroundUrl = `${origin}/images/backgrounds/${searchParams.get('b') || 1}.jpeg`;
	const playerName = searchParams.get('n') || '';
	const defeatedEnemies = searchParams.get('s') || '';
	const gameName = 'Dice Quest';
	const imageData = await generateResultImage(
		avatarUrl,
		backgroundUrl,
		playerName,
		defeatedEnemies,
		gameName
	);
	const image = Buffer.from(imageData, 'base64');



	return new Response(image, {
		status: 200,
		headers: {
			'Content-Type': 'image/png',
			'Content-Length': image.length,
			'Content-Disposition':
				// Use filename* instead of filename to support non-ASCII characters
				`attachment; filename*=UTF-8''image.png`
		}
	});
}
